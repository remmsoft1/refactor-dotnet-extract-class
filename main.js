const fs = require("fs");
const lineReader = require("line-reader");
const globby = require("globby");
const path = require("path");

function generateFiles(fileName, outputPath) {
  let myClass = "";
  let myClassName = "";
  let using = "";

  lineReader.eachLine(
    fileName,
    function (line) {
      classIndex = line.indexOf("class");
      if (classIndex > -1) {
        if (myClassName == "") {
          using = myClass;
        } else {
          createFile(myClassName, using + myClass + "}", outputPath);
        }

        nextBlankCharIndex = line.indexOf(" ", classIndex + 8);
        if (nextBlankCharIndex > -1) {
          myClassName = line.substring(classIndex + 6, nextBlankCharIndex);
        } else {
          myClassName = line.substring(
            classIndex + 6,
            classIndex + line.length
          );
        }

        myClass = line + "\n";
      } else if (
        line.indexOf("#region") == -1 &&
        line.indexOf("#endregion") == -1
      ) {
        myClass = myClass + line + "\n";
      }
    },
    function (err) {
      if (err) throw err;
      createFile(myClassName, using + myClass, outputPath);
    }
  );
}

function createFile(fileName, content, outputPath) {
  //   console.log("fn: " + fileName);
  //   console.log(content);
  const newFileName = path.join(outputPath, fileName + ".cs");
  console.log(newFileName);
  ensureDirectoryExistence(newFileName);

  fs.writeFile(newFileName, content, function (err) {
    if (err) throw err;
    // console.log("File is created successfully.");
  });
}

function ensureDirectoryExistence(filePath) {
  var dirname = path.dirname(filePath);
  if (fs.existsSync(dirname)) {
    return true;
  }
  ensureDirectoryExistence(dirname);
  fs.mkdirSync(dirname);
}

const listAllFilesAndDirs = (dir) => globby(`${dir}/**/*`);

(async () => {
  const fileList = await listAllFilesAndDirs(process.cwd() + "/input");
  const baseDir = process.cwd() + "/input";
  fileList.forEach((fileName) => {
    // console.log(fileName);
    const newPath = path.dirname(fileName).replace(baseDir, "");
    const outputPath = path.join(path.resolve("./output/"), newPath);
    const outputFileName = path.join(outputPath, path.basename(fileName));
    // console.log(outputFileName);
    // console.log(path.dirname(fileName));
    generateFiles(fileName, outputPath);
  });
})();
